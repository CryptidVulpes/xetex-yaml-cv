# XeTeX Curriculum Vitae (Themed by Awesome-CV)
Easy to use YAML-templated, Awesome CV themed, CV boilerplate with integration to GitLab CI and Pages.

Inspired by Dimitrie Hoekstra's 
[curriculumvitae-ci-boilerplate](https://gitlab.com/dimitrieh/curriculumvitae-ci-boilerplate/tree/master) which is based
upon Mattia Tezzele's [tex-boilerplates](https://github.com/mrzool/cv-boilerplate) which is based on Dario Taraborelli's
[cvtex](https://github.com/dartar/cvtex)

Uses [Awesome CV template](https://github.com/posquit0/Awesome-CV) by Byungjin Park

## Résumé 
See: https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/resume.pdf

![resume](https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/resume.png)

## CV
See: https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/cv.pdf

![cv](https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/cv.png)

## Coverletter
See: https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/coverletter.pdf

![cv](https://cryptidvulpes.gitlab.io/xetex-yaml-cv/awesome-cv/coverletter.png)

## Dependencies
Tested distribution is Ubuntu Zesty.

### Docker Image

```bash
docker pull registry.gitlab.com/cryptidvulpes/docker-latex:awesomecv
```

### Packages

```bash
sudo apt-get install texlive-xetex texlive-latex-extra texlive-math-extra texlive-latex-recommended \
                     texlive-fonts-extra fonts-font-awesome fonts-roboto make pandoc
```

## Usage
It is recommended to use `.gitlab-ci.yml` integration, but this template should be usable on your local machine or from
within a Docker Image with the correct dependencies.

### Using prebuilt Docker Image with GitLab CI
Create a new repository for yourself. Add files `cv.yml` and
`resume.yml` (You can copy them from this repository as well). Update
them with your information.  Add `.gitlab-ci.yml` that looks like this:


```yaml
image: registry.gitlab.com/cryptidvulpes/xetex-yaml-cv:latest

stages:
  - build
  - deploy

build:
  stage: build
  script:
    - cp -v --recursive /xetex-yaml-cv/* /builds/$CI_PROJECT_PATH
    - make all
  artifacts:
    when: on_success
    paths:
      - artifacts/

pages:
  image: busybox
  stage: deploy
  dependencies:
    - build
  script:
    - mv artifacts public
  artifacts:
    paths:
      - public
```

### Forking repository
You can also fork this repository and update `cv.yml` and `resume.yml`
with your information.

### Building locally
You can download this repository as a zip or `git clone` it locally and
update `cv.yml` and `resume.yml` and build your PDFs:

```bash
make all
```

### YAML-file fields
#### Personal Details
Mandatory Fields:

* `firstname`
* `lastname`
* `address`
* `mobile`
* `email`

Optional Fields:

* `position`
* `homepage`
* `github`
* `gitlab`
* `linkedin`
* `stackoverflow`
    * `id`
    * `name`
* `twitter`
* `skype`
* `reddit`
* `extrainfo`
* `quote`

#### (optional) Overview
Mandatory Fields:
* `overviewtitle`
* `overview`

#### (optional) Education
Mandatory Fields:
* `educationtitle`

Optional Fields:
* `education`
    * `degree`
    * `institution`
    * `location`
    * `period`
    * (optional) `description`

#### (optional) Work History
Mandatory Fields:
* `experiencetitle`

Optional Fields:
* `experience`
    * `title`
    * `organization`
    * `location`
    * `period`
    * (optional) `description`

#### (optional) Extracurricular Activities
Mandatory Fields:
* `extracurriculartitle`

Optional Fields:
* `extracurricular`
    * `role`
    * `organization`
    * `location`
    * `period`
    * (optional) `description`

#### (optional) Skills
Mandatory Fields:
* `skillstitle`

Optional Fields:
* `skill`
    * `category`
    * `skills`

#### (optional) Settings
* `sectionorder` (list of `sections/*.tex` files)
    * For example:
```
sectionorder:
  - education.tex
  - extracurricular.tex
  - experience.tex
  - skills.tex
```
* `templatecolor` (string of `awesome-cv.cls` color settings)
    * Valid values listed in [`awesome-cv.cls:130-138`](https://gitlab.com/CryptidVulpes/xetex-yaml-cv/blob/master/themes/awesome-cv.cls#L130-138)

### Adding new themes
Create a file called `themes/<theme>.cls` and add a corresponding template to `templates/<theme>.tex`. If the theme
does not support the same LaTeX commands, you need to edit the files in `sections` or create a new directory
and modify the `Makefile` line `SECTIONS := $(wildcard sections/*.tex)` to point to the correct sections.

#### LaTeX Commands required

* `\cvsection{}`
* `\begin{cventries}`
    * `\cventry{}{}{}{}{}`
    * `\begin{cvitems}`
        * `\item{}`
    * `\end{cvitems}`
* `\end{cventries}`
* `\begin{cvskills}`
    * `\cvskill{}{}`
* `\end{cvskills}`
* All the commands in `sections/personal.tex`
