TEX = pandoc
TEXPP = arlatex
THEME ?= awesome-cv

SECTIONS ?= $(wildcard $(CURDIR)/sections/*.tex)
BUILDDIR ?= artifacts

SOURCES ?= $(wildcard $(CURDIR)/*.yml)
TARGETS ?= $(patsubst $(CURDIR)/%.yml,$(BUILDDIR)/$(THEME)/%.pdf,$(SOURCES))

.PHONY: clean
clean:
	rm -frv $(BUILDDIR)

# Directory like artifacts/awesome-cv
$(BUILDDIR)/$(THEME):
	mkdir -p $@

# Files like artifacts/awesome-cv/full.tex
$(BUILDDIR)/%/full.tex: $(CURDIR)/templates/%.tex $(CURDIR)/themes/$(THEME).cls $(SECTIONS) $(BUILDDIR)/$(THEME)
	$(TEXPP) --outfile=$@ --document=$< $(CURDIR)/themes/$(THEME).cls $(SECTIONS)

# Files like artifacts/awesome-cv/cv.pdf
$(TARGETS): $(BUILDDIR)/$(THEME)/%.pdf : $(CURDIR)/%.yml $(BUILDDIR)/$(THEME)/full.tex
	$(TEX) -o $@ --latex-engine=xelatex --template=$(BUILDDIR)/$(THEME)/full.tex $<

.PHONY: all
all: $(TARGETS)
