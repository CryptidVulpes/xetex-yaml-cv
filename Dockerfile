FROM registry.gitlab.com/cryptidvulpes/docker-latex:awesomecv
MAINTAINER CryptidVulpes

ADD . /xetex-yaml-cv
RUN rm -v /xetex-yaml-cv/cv.yml /xetex-yaml-cv/resume.yml /xetex-yaml-cv/coverletter/coverletter.yml
